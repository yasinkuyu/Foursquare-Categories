﻿using System;
using System.IO;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;

namespace FoursquareCategories
{
    class Program
    {
        private const string constr = "Server=localhost;Uid=root;Pwd=root;Database=boylam;";

        public static void Recursive(JToken item, long lastid)
        {

            var db = new MySqlConnection(constr);
            db.Open();
            var cmd = db.CreateCommand();
            foreach (var subitem in item["categories"])
            {
                cmd.CommandText = string.Format("INSERT INTO category2(`Name`, `ParentId`, `fid`) VALUES('{0}', '{1}', '{2}')", subitem["name"].ToString().Replace("'", "`"), lastid, subitem["id"]);
                cmd.ExecuteNonQuery();
                Console.WriteLine("\t --name: {0}", subitem["name"]);
                if (subitem["categories"] != null)
                    Recursive(subitem, cmd.LastInsertedId);
            }
            db.Close();

        }

        static void Main(string[] args)
        {

            var db = new MySqlConnection(constr);
            db.Open();
            var cmd = db.CreateCommand();

            var file = Path.GetFullPath(@"foursquare-categories.json");
            var json = File.ReadAllText(file);
            var data = JObject.Parse(json);

            foreach (var item in data["categories"])
            {
                cmd.CommandText = string.Format("INSERT INTO category2(`Name`, `ParentId`, `fid`) VALUES('{0}', '{1}', '{2}')", item["name"], 0, item["id"]);
                cmd.ExecuteNonQuery();
                Console.WriteLine("name: {0}", item["name"]);
                Recursive(item, cmd.LastInsertedId);
            }
            db.Close();

            Console.ReadKey();

        }

    }
}
