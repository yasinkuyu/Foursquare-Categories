Foursquare Categories
====================

Foursquare categories recursive MySQL Database insert with C#


First dump foursquare-categories.json ----> https://gist.github.com/janosgyerik/2906942

File structure 

	{
		"categories": [],
		"icon": {
			"prefix": "https://ss1.4sqi.net/img/categories_v2/arts_entertainment/artgallery_",
			"suffix": ".png"
		},
		"id": "4bf58dd8d48988d1e2931735",
		"name": "Art Gallery",
		"pluralName": "Art Galleries",
		"shortName": "Art Gallery"
	}


Create the database.

	CREATE TABLE `category` (
		`CatId` int(11) NOT NULL AUTO_INCREMENT,
		`ParentId` int(11) DEFAULT NULL,
		`Name` char(255) DEFAULT NULL,
		`fid` varchar(255) DEFAULT NULL,
	PRIMARY KEY (`CatId`)
	) ENGINE=MyISAM AUTO_INCREMENT=8995 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;


or python version check my gist.

yasinkuyu / foursquare.py
https://gist.github.com/yasinkuyu/51312f1c1ffe9a59be8c